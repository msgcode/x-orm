package com.xdivo.orm.handler;

/**
 * 操作失败回调
 * Created by jaleel on 16-11-20.
 */
public interface DataFailHandler {
    void handle(long result);
}
