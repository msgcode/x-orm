package com.xdivo.orm.handler;

/**
 * 操作成功回调
 * Created by jaleel on 16-11-20.
 */
public interface DataSuccessHandler {
    void handle(long result);
}
