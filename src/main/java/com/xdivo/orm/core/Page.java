package com.xdivo.orm.core;

import java.io.Serializable;
import java.util.List;

/**
 * 分页相关对象
 * Created by jaleel on 16-10-5.
 */
public class Page<T> implements Serializable{

    private static final long serialVersionUID = -1658563369597538418L;

    private int pageNum; //页码

    private int pageSize; //每页记录数

    private List<T> data; //数据

    private int totalSize; //总记录数

    private int totalPage; //总页数

    public Page(){}

    public Page(Integer pageNum, Integer pageSize) {
        if(null == pageNum) {
           this.pageNum = 0;
        }else {
            this.pageNum = pageNum;
        }
        if(null == pageSize) {
            this.pageSize = 0;
        }else {
            this.pageSize = pageSize;
        }
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }
}
