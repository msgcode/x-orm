package com.xdivo.orm.exception;

/**
 * 找不到关联模型异常
 * Created by jaleel on 16-10-5.
 */
public class RefModelNotFoundException extends Exception{
    public RefModelNotFoundException(String message) {
        super(message);
    }
}
