package com.xdivo.orm.exception;

/**
 * 找不到Model异常
 * Created by jaleel on 16-10-5.
 */
public class ModelNotFoundException extends Exception{
}
