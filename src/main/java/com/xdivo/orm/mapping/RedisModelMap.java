package com.xdivo.orm.mapping;

import java.util.List;

/**
 * Model映射
 * Created by liujunjie on 16-7-23.
 */
public class RedisModelMap {

    //对应表名
    private String key;

    //序列化方式
    private int serialType;

    //属性与数据库字段
    private List<ColumnMap> columnMaps;

    //关联Models
    private List<JoinMap> joinMaps;


}
