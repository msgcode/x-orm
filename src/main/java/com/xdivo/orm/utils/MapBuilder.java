package com.xdivo.orm.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Map Builder
 * Created by jaleel on 16-10-1.
 */
public class MapBuilder {

    private Map<String, Object> map = new HashMap<>();

    public MapBuilder add(String key, Object value) {
        map.put(key, value);
        return this;
    }

    public Map<String, Object> build() {
        return map;
    }
}
